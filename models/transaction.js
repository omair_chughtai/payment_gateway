const mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define transaction Schema

var TransactionSchema = new mongoose.Schema({
	transactionId: {
		type: String,
		required: true
    },
    transactionStatus: {
        type: String,
        required: false,
    },
    errorCode: {
        type: Number,
        required: false
    },
    errorMessage: {
        type: String,
        required: false
    },
    invoiceId: {
        type: String,
        required: true
    },
	merchantId: {
		type: String,
		required: true
	},
	customerId: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true
	},
	paymentStatus: {
		type: String,
		required: true
	},
	paymentType: {
		type: String,
		required: true
	},
	currencyIsoCode: {
		type: String,
		required: true
	},
	createdAt: {
		type: Date,
		required: true
	},
	updatedAt: {
		type: Date,
		required: true
	}
});

module.exports = mongoose.model('transaction', TransactionSchema, 'transactions');
