var payment = require('./payment');
var transaction = require('./transaction');
var braintree = require('braintree');

var gateway = braintree.connect({
	// All ID's and Keys are currently from Omair's Account
	environment: braintree.Environment.Sandbox,
	merchantId: 'tg4v3h82g55vp6tf',
	publicKey: 'j9g8dpxv99trpkdy',
	privateKey: '9591d4cb8f888339b9fa78b7154b3a0c'
});

// Constants (should be moved to flexbiz constants file)
const DEFAULT_RETRY_PERIOD = 'in 10 seconds';
//const DEFAULT_RETRY_PERIOD = 'in 24 hours';
//const STATUS_PERIOD = 'in 12 hours';
const STATUS_PERIOD = 'in 10 seconds';
//const SETTLEMENT_PERIOD = 'in 72 hours';
const SETTLEMENT_PERIOD = 'in 10 seconds';
const NUM_RETRY = 3;

module.exports.deleteCustomer = function(data, callback) {
    gateway.customer.delete(data.customer_id, function (err) {
        if (err) {
            res.status(400).send({error: err});
            console.log("Error: " + err);
            return;
        }
        console.log("Sucessfully deleted customer object");
        res.status(200).send({success: "true"});
      });
}

module.exports.deletePayment = function(data, callback) {
   gateway.paymentMethod.delete(data.cctoken, function (err) {
        if (err) {
            res.status(400).send({error: err});
            console.log("Error: " + err);
            return;
        }
        console.log("Sucessfully deleted payment method");
        res.status(200).send({success: "true"});
    }); 
}

module.exports.addPayment = function(data, callback) {
	var resp = data;
	var userId = data.userId;
	var nonce = data.nonce;

	gateway.customer.create(
		{
			paymentMethodNonce: nonce,
			creditCard: {
				options: {
					verifyCard: true
				}
			}
		},
		function(err, result) {
			if (err || result.success == false) {
				// No retries for this
				handleError(result);
				callback(result.message, null);
				return;
			}
			console.log('customer created successfully');
			var customerId = result.customer.id;
			var paymentToken = result.customer.paymentMethods[0].token;

			console.log('customerId: ' + customerId);
			console.log('paymentToken: ' + paymentToken);

			payment
				.update(
					{ userId: userId, customerId: customerId, paymentToken: paymentToken },
					{ userId: userId, customerId: customerId, paymentToken: paymentToken },
					{ upsert: true }
				)
				.then(function(result, err) {
					if (err) {
						console.log(err);
						callback(err, null);
					} else {
						console.log(result);
						callback(null, resp);
					}
				});
		}
	);
};

function chargeCustomer(agenda, data, customerId, amount) {
	gateway.transaction.sale(
		{
			customerId: customerId,
			amount: amount
		},
		function(err, result) {
			if (err || result.success == false) {
				// Check error codes to see if retry is appropriate
				var retry = handleError(result);

				if (retry && data.attempt < NUM_RETRY) {
					agenda.schedule(DEFAULT_RETRY_PERIOD, 'chargeCustomers', { attempt: data.attempt + 1 });
				} else if (!retry || data.attempt >= NUM_RETRY) {
					// Create failed Transaction entry in db
					var errorCode = null;

					if (result.verification != null) {
						errorCode = result.verification.processorResponseCode;
					}

					transaction.create(
						{
							invoiceId: '1234567890', // Change to match invoice object id
							transactionId: result.transaction.id,
							transactionStatus: 'failed',
							merchantId: result.transaction.merchantAccountId,
							customerId: result.transaction.customer.id,
							amount: result.transaction.amount,
							paymentStatus: result.transaction.status,
							paymentType: result.transaction.type,
							currencyIsoCode: result.transaction.currencyIsoCode,
							createdAt: result.transaction.createdAt,
							updatedAt: result.transaction.updatedAt,
							errorMessage: result.message,
							errorCode: errorCode
						},
						function(err, res) {
							if (err) {
								console.log(err);
							} else {
								console.log(res);
							}
						}
					);
					// TODO: Alert Admins
				}
				return;
			}
			console.log('charge success');

			// add details to transaction db
			transaction
				.update(
					{
						invoiceId: '1234567890', // Change to match invoice object id
						transactionId: result.transaction.id,
						merchantId: result.transaction.merchantAccountId,
						customerId: result.transaction.customer.id,
						amount: result.transaction.amount,
						paymentStatus: result.transaction.status,
						paymentType: result.transaction.type,
						currencyIsoCode: result.transaction.currencyIsoCode,
						createdAt: result.transaction.createdAt,
						updatedAt: result.transaction.updatedAt
					},
					{
						transactionId: result.transaction.id,
						merchantId: result.transaction.merchantAccountId,
						customerId: result.transaction.customer.id,
						amount: result.transaction.amount,
						paymentStatus: result.transaction.status,
						paymentType: result.transaction.type,
						currencyIsoCode: result.transaction.currencyIsoCode,
						createdAt: result.transaction.createdAt,
						updatedAt: result.transaction.updatedAt
					},
					{ upsert: true }
				)
				.then(function(result, err) {
					if (err) {
						console.log(err);
					} else {
						console.log(result);
					}
				});

			// Schedule the settlement job for 72 hours from now
			agenda.schedule(SETTLEMENT_PERIOD, 'submitSettlement', {
				transactionId: result.transaction.id,
				attempt: 1
			});
		}
	);
}

module.exports.chargeCustomers = function(agenda, data, callback) {
	payment.find({}).then(function(result, err) {
		if (err) {
			console.log(err);
			callback(err, null);
		} else {
			console.log(result);
			result.forEach(function(item) {
				chargeCustomer(agenda, data, item.customerId, 5);
			});
		}
	});
};

function submitSettlement(transactionId, callback) {
	gateway.transaction.submitForSettlement(transactionId, function(err, result) {
		if (err) {
			callback(err, null);
		} else {
			callback(null, result);
		}
	});
}

module.exports.submitSettlement = function(agenda, data, callback) {
	console.log('\n-----Submit Transaction for Settlement-----\n');
	submitSettlement(data.transactionId, function(err, result) {
		if (err || result.success == false) {
			var retry = handleError(result);

			if (retry && data.attempt < NUM_RETRY) {
				agenda.schedule(DEFAULT_RETRY_PERIOD, 'submitSettlement', {
					transactionId: result.transaction.id,
					attempt: data.attempt + 1
				});
			} else if (!retry || data.attempt >= NUM_RETRY) {
				// Create failed Transaction entry in db
				var errorCode = null;

				if (result.verification != null) {
					errorCode = result.verification.processorResponseCode;
				}

				transaction
					.update(
						{
							transactionId: result.transaction.id,
							merchantId: result.transaction.merchantAccountId,
							customerId: result.transaction.customer.id,
							amount: result.transaction.amount,
							paymentType: result.transaction.type,
							currencyIsoCode: result.transaction.currencyIsoCode,
							createdAt: result.transaction.createdAt
						},
						{
							transactionId: result.transaction.id,
							transactionStatus: 'failed',
							merchantId: result.transaction.merchantAccountId,
							customerId: result.transaction.customer.id,
							amount: result.transaction.amount,
							paymentStatus: result.transaction.status,
							paymentType: result.transaction.type,
							currencyIsoCode: result.transaction.currencyIsoCode,
							createdAt: result.transaction.createdAt,
							updatedAt: result.transaction.updatedAt,
							errorMessage: result.message,
							errorCode: errorCode
						}
					)
					.then(function(result, err) {
						if (err) {
							console.log(err);
						} else {
							console.log(result);
						}
					});

				// TODO: Alert Admins
				return;
			}

			callback(result.message, null);
			return;
		} else if (result.success == true || result.transaction.status == 'submitted_for_settlement') {
			console.log('Transaction ID: ' + data.transactionId + '   Attempt: ' + data.attempt);
			console.log('Success: ' + result.success);
			console.log('Transaction Status: ' + result.transaction.status);

			// Find previous transaction and update
			transaction
				.update(
					{
						transactionId: result.transaction.id,
						merchantId: result.transaction.merchantAccountId,
						customerId: result.transaction.customer.id,
						amount: result.transaction.amount,
						paymentType: result.transaction.type,
						currencyIsoCode: result.transaction.currencyIsoCode,
						createdAt: result.transaction.createdAt
					},
					{
						transactionId: result.transaction.id,
						merchantId: result.transaction.merchantAccountId,
						customerId: result.transaction.customer.id,
						amount: result.transaction.amount,
						paymentStatus: result.transaction.status,
						paymentType: result.transaction.type,
						currencyIsoCode: result.transaction.currencyIsoCode,
						createdAt: result.transaction.createdAt,
						updatedAt: result.transaction.updatedAt
					}
				)
				.then(function(result, err) {
					if (err) {
						console.log(err);
					} else {
						console.log(result);
					}
				});

			// Schedule status check job
			agenda.schedule(STATUS_PERIOD, 'checkStatus', { transactionId: result.transaction.id, attempt: 1 });
			return;
		}
	});
};

function checkStatus(transactionId, callback) {
	gateway.transaction.find(transactionId, function(err, transaction) {
		if (err) {
			callback(err, null);
		} else {
			callback(null, transaction);
		}
	});
}

module.exports.checkStatus = function(agenda, data, callback) {
	console.log('\n-----Check Transaction Status-----\n');
	checkStatus(data.transactionId, function(err, transactionResult) {
		if (err) {
			console.log('Error: ' + err);
		} else {
			console.log('Transaction ID: ' + transactionResult.id + '    Attempt: ' + data.attempt);

			if (transactionResult.status == 'submitted_for_settlement') {
				console.log('submitted_for_settlement: Re-check status in 12 hr');
				// Schedule retry status job for 1 hr from now
				agenda.schedule(STATUS_PERIOD, 'checkStatus', {
					transactionId: transactionResult.id,
					attempt: data.attempt + 1
				}); // Currently set to schedule 10 seconds from now
			} else if (transactionResult.status == 'settled') {
				console.log('settled: Transaction complete');

				// Find previous transaction and update
				transaction
					.update(
						{
							transactionId: transactionResult.id,
							customerId: transactionResult.customer.id
						},
						{
							transactionId: transactionResult.id,
							transactionStatus: 'success',
							merchantId: transactionResult.merchantAccountId,
							customerId: transactionResult.customer.id,
							amount: transactionResult.amount,
							paymentStatus: transactionResult.status,
							paymentType: transactionResult.type,
							currencyIsoCode: transactionResult.currencyIsoCode,
							createdAt: transactionResult.createdAt,
							updatedAt: transactionResult.updatedAt
						}
					)
					.then(function(result, err) {
						if (err) {
							console.log(err);
						} else {
							console.log(result);
						}
					});
			} else {
				console.log('Status is something other then submitted or settled: ' + transactionResult.status);
				// Create failed Transaction entry in db

				transaction
					.update(
						{
							transactionId: transactionResult.id,
							customerId: transactionResult.customer.id
						},
						{
							transactionId: transactionResult.id,
							transactionStatus: 'failed',
							merchantId: transactionResult.merchantAccountId,
							customerId: transactionResult.customer.id,
							amount: transactionResult.amount,
							paymentStatus: transactionResult.status,
							paymentType: transactionResult.type,
							currencyIsoCode: transactionResult.currencyIsoCode,
							createdAt: transactionResult.createdAt,
							updatedAt: transactionResult.updatedAt
						}
					)
					.then(function(result, err) {
						if (err) {
							console.log(err);
						} else {
							console.log(result);
						}
					});
				// TODO: Alert admins
				return;
			}
		}
	});
};

/*
 * Handles the errors thrown by Braintree. Returns true if retry is appropriate. Returns false if retry is not appropriate. 
 */

function handleError(result) {
	console.log('\n------ Handling an Error ------\n');

	if (result.verification != null) {
		console.log(
			'\nVerification Status: ' +
				result.verification.status +
				' \nResponse Text: ' +
				result.verification.processorResponseText +
				'\nStatus Code: ' +
				result.verification.processorResponseCode
		);

		if (result.verification.processorResponseCode >= 2000 && result.verification.processorResponseCode <= 2003) {
			return true;
		}
	} else if (result.transaction != null && result.transaction.status == 'gateway_rejected') {
		console.log(result.message);
	} else {
		console.log('\n----- OTHER ERROR -----\n');
	}

	return false;
}
