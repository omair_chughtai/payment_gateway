const mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define payment Schema

var PaymentSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: true
	},
	customerId: {
		type: String,
		required: true
	},
	paymentToken: {
		type: String,
		required: true
	}
});

module.exports = mongoose.model('payment', PaymentSchema);
