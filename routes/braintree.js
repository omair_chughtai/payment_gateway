// Authored by Omair Chughtai for Stratus Silver Lining

const express = require('express');
var braintree = require('../controllers/braintreeController');

var router = express.Router();

router
    .route('/client_token')
        .post(braintree.generateClientToken);

router
    .route('/client_token_new')
        .get(braintree.generateNewClientToken);
        
router
    .route('/createCustomer')
        .post(braintree.createCustomer);

router
    .route('/deleteCustomer')
        .post(braintree.deleteCustomer);

router
    .route('/updateCustomer')
        .post(braintree.updateCustomer);

router
    .route('/addPaymentMethod')
        .post(braintree.addPaymentMethod);

router
    .route('/deletePaymentMethod')
        .post(braintree.deletePaymentMethod);

router
    .route('/chargeCustomerId')
        .post(braintree.chargeCustomerId);

router
    .route('/settleTransaction')
        .post(braintree.settleTransaction);

router
    .route('/statusTransaction')
        .post(braintree.transactionStatus);

router
    .route('/voidTransaction')
        .post(braintree.voidTransaction);

module.exports = router;