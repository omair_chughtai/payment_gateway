const express = require('express');
var router = express.Router();
var payment = require('../controllers/paymentController');

router.route('/addPayment').post(payment.addPayment);
router.route('/deletePayment').post(payment.deletePayment);

module.exports = router;
