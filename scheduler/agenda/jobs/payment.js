var payment = require('../../../models/paymentTable');

module.exports = function(agenda) {
	agenda.define('chargeCustomers', function(job) {
		payment.chargeCustomers(agenda, job.attrs.data, function(err, result) {
			if (err) {
				console.log(err);
			} else {
				console.log(result);
				console.log('Submit Settlement Job Scheduled');
			}
		});
	});

	agenda.define('submitSettlement', function(job) {
		payment.submitSettlement(agenda, job.attrs.data, function(err, result) {
			if (err) {
				console.log(err);
			} else {
				console.log(result);
			}
		});
	});

	agenda.define('checkStatus', function(job) {
		payment.checkStatus(agenda, job.attrs.data, function(err, result) {
			if (err) {
				console.log(err);
			} else {
				console.log(result);
			}
		});
	});
};
