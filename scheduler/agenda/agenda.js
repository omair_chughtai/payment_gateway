var Agenda = require('agenda');

var mongoConnectionString = 'mongodb://127.0.0.1/agenda-flexbiz';

var agenda = new Agenda({ db: { address: mongoConnectionString, collection: 'billingJobs' } });

console.log('agenda here');

// Set environment to job types (DELETE LATER)
process.env.JOB_TYPES = 'payment';

var jobTypes = process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [];

jobTypes.forEach(function(type) {
	require('./jobs/' + type)(agenda);
});

if (jobTypes.length) {
	agenda.on('ready', () => {
		agenda.start();
		console.log('agenda ready');
	});
}

module.exports = agenda;
