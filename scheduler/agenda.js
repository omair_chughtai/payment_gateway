const mongoose = require('mongoose');
const Agenda = require('agenda');
const braintreeController = require('../controllers/braintreeController');

// Create Agenda Scheduling System
var agendaMongoConnectionString = 'mongodb://127.0.0.1/agenda-dev';
var agenda = new Agenda({db: {address: agendaMongoConnectionString, collection: 'billingJobs'}});

// Create Database Connection
/*
mongoose.connect('mongodb://127.0.0.1/transactions');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connection to DB Established");
});
*/

// Import Transaction Model for MongoDB
var transactionModel = require('../models/transaction');



/*
 * Agenda test function to test scheduling of tasks
 */

agenda.define('test', function (job, done) {
    console.log("Test Success");
});



/*
 * Agenda function to remove jobs from agenda job database upon completion
 *  (Causes strange errors if not used, sometimes restarting old jobs)
 */

agenda.on('complete', function(job) { 
    //    console.log("Job finished");
        job.remove(function(err) {
    //        console.log(err); //prints null
       });
    });
    


/*
 * Agenda job to charge the customer and authorize a transaction on their default payment method
 */

agenda.define('chargeCustomer', function (job, done) {

    console.log("\n-----Charging Customer-----\n");

    braintreeController.chargeCustomerIdAgenda(job.attrs.data, function(err, result) {

        if (err) {
            console.log("Error: " + err);
            // TODO: Check deep error codes to see if retry is appropriate
            // https://articles.braintreepayments.com/control-panel/transactions/declines
            // https://developers.braintreepayments.com/reference/general/validation-errors/overview/ruby
                    // TODO: Schedule Retry transaction job for 1hr from now
            
            // TODO: Else void transaction and report to admins
        }
        else {
            console.log("Success: " + result.success);
            console.log("TransactionId: " + result.transaction.id);

            // Add entry to MongoDB: transactions/authorized
            var authorizedTransaction = new transactionModel.authorize({
                transactionId: result.transaction.id,
                merchantId: result.transaction.merchantAccountId,
                customerId: result.transaction.customer.id,
                amount: result.transaction.amount,
                paymentStatus: result.transaction.status,
                paymentType: result.transaction.type,
                currencyIsoCode: result.transaction.currencyIsoCode, 
                createdAt: result.transaction.createdAt, 
                updatedAt: result.transaction.updatedAt
            });

            authorizedTransaction.save(function (err) {
                if (err) {
                    console.log (err);
                } else {
                    console.log("Successfully Created Authorized Document");
                }
            });

            // Schedule the settlement job for 72 hours from now
            agenda.schedule('in 10 seconds', 'settlement', {transaction_id: result.transaction.id, attempt: 1}); // Currently set to schedule 10 seconds from now
        }
    });
    done();
});


/*
 * Function to submit authorized transactions for settlement and schedule transaction status check job
 */

agenda.define('settlement', function (job, done) {
    console.log('\n-----Submit Transaction for Settlement-----\n');
    braintreeController.settleTransactionAgenda(job.attrs.data, function (err, result) {
        if (err) {
            console.log("Error: " + err);
            // TODO: Check deep error codes to see if retry is appropriate
            // https://articles.braintreepayments.com/control-panel/transactions/declines
            // https://developers.braintreepayments.com/reference/general/validation-errors/overview/ruby
                    
                    // TODO: Schedule retry transaction job for 3 hr from now

            // TODO: Else void transaction and report to admins
        }

        else if (result.success == true || result.transaction.status == 'submitted_for_settlement') {
            
                console.log("Transaction ID: " + job.attrs.data.transaction_id + "    Attempt: " + job.attrs.data.attempt);
                console.log("Success: " + result.success);
                console.log("Transaction Status: " + result.transaction.status);

                // Delete authorized entry from MongoDB
                transactionModel.authorize.deleteOne({transactionId: result.transaction.id}, function (err) {
                    if (err) {
                        console.log("Delete Authorized Document Error: " + err);
                    } else {
                        console.log("Successfully deleted Authorized Document");
                    }
                });

                // Create settlment submission entry in MongoDB
                var settleTransaction = new transactionModel.submitted_for_settlement({
                    transactionId: result.transaction.id,
                    merchantId: result.transaction.merchantAccountId,
                    customerId: result.transaction.customer.id,
                    amount: result.transaction.amount,
                    paymentStatus: result.transaction.status,
                    paymentType: result.transaction.type,
                    currencyIsoCode: result.transaction.currencyIsoCode, 
                    createdAt: result.transaction.createdAt, 
                    updatedAt: result.transaction.updatedAt
                });

                settleTransaction.save(function (err) {
                    if (err) {
                        console.log (err);
                    } else {
                        console.log("Successfully saved Settlement Document");
                    }
                });

                // Schedule status check job (check every hour?)
                agenda.schedule('in 10 seconds', 'checkStatus', {transaction_id: result.transaction.id, attempt: 1}); // Currently set to schedule 10 seconds from now
        } 
        else {
            // TODO: Outlier situations (i.e voided...etc)
            console.log("Success = " + result.success + "   Transaction Status = " + result.transaction.status);
        }
    });
    done();
});


/*
 *  Function to check status of transactions submitted for settlement and persist all relevant data
 */

agenda.define('checkStatus', function (job, done) {
    console.log('\n-----Check Status of Submitted for Settlement Transaction-----\n');
    braintreeController.transactionStatusAgenda(job.attrs.data, function (err, transaction) {
        if (err) {
            console.log("Error: " + err);
            // TODO: Check deep error codes to see if retry is appropriate (retry should always be appropriate, only real potential for error here is a malformed transactionId)
            // https://articles.braintreepayments.com/control-panel/transactions/declines
            // https://developers.braintreepayments.com/reference/general/validation-errors/overview/ruby
                    // TODO: Schedule retry status job for 1 hr from now
        }
        else {
            console.log("Transaction ID: " + job.attrs.data.transaction_id + "    Attempt: " + job.attrs.data.attempt);

            if (transaction.status == 'submitted_for_settlement') {
                console.log("submitted_for_settlement: Re-check status in 1 hr");
                // Schedule retry status job for 1 hr from now
                agenda.schedule('in 10 seconds', 'checkStatus', {transaction_id: transaction.id, attempt: job.attrs.data.attempt + 1}); // Currently set to schedule 10 seconds from now
            }

            else if (transaction.status == 'settled'){
                console.log('settled: Transaction complete');

                // Remove settlement submission entry in MongoDB
                transactionModel.submitted_for_settlement.deleteOne({transactionId: transaction.id}, function (err) {
                    if (err) {
                        console.log("Delete Submitted for Settlement Document Error: " + err);
                    } else {
                        console.log("Successfully deleted Submitted for Settlement Document");
                    }
                });

                // Create settled entry in MongoDB
                var settledTransaction = new transactionModel.settled({
                    transactionId: transaction.id,
                    merchantId: transaction.merchantAccountId,
                    customerId: transaction.customer.id,
                    amount: transaction.amount,
                    paymentStatus: transaction.status,
                    paymentType: transaction.type,
                    currencyIsoCode: transaction.currencyIsoCode, 
                    createdAt: transaction.createdAt, 
                    updatedAt: transaction.updatedAt
                });

                settledTransaction.save(function (err) {
                    if (err) {
                        console.log (err);
                    } else {
                        console.log("Successfully saved Settlment Document");
                    }
                });
            }

            else {
                // TODO: Check deep result codes to see if retry is appropriate 
                    // TODO: Schedule retry status job for 1 hr from now
                // TODO: Take action appropriate to status code
                console.log("Status is something other then submitted or settled: " + transaction.status);
            }
        }
    });
    done();
});


/*
 * Starts the agenda scheduler, comment out test sections as needed
 */ 

agenda.on('ready', function () {
    // Test customer charge function
//    agenda.now('chargeCustomer', {customer_id: 129536849, amount: 10.00, attempt: 1});

    // Test submit for settlement function
//    agenda.now('settlement', {transaction_id: 'f6fxxcvq', attempt: 1});

    // Test status check function (two different transactions)
//    agenda.now('checkStatus', {transaction_id: '0041gtbp', attempt: 1});
//    agenda.now('checkStatus', {transaction_id: 'eamj6qk1', attempt: 1});
//    agenda.now('checkStatus', {transaction_id: '83xhsxzz', attempt: 1});

    agenda.start();
});

module.exports = agenda;
