// Authored by Omair Chughtai for Stratus Silver Lining

var braintree = require("braintree");

// Establish Connection to Braintree using ID and API Keys

var gateway = braintree.connect({ // All ID's and Keys are currently from Omair's Account
  environment: braintree.Environment.Sandbox,
  merchantId: "tg4v3h82g55vp6tf",
  publicKey: "j9g8dpxv99trpkdy",
  privateKey: "9591d4cb8f888339b9fa78b7154b3a0c"
});

/*
 ************************ API FUNCTIONS ************************
*/

/* Function to generate ClientToken for existing customer 
 *
 * Post Body Parameters:
 * customer_id
 * res parameters:
     "clientToken": string
 */ 

module.exports.generateClientToken = function(req, res) {
    gateway.clientToken.generate({
      customerId: req.body.customer_id
      }, function (err, response) {
            if (err || response.clientToken == undefined) {
                console.log("Failed to locate customer");
                res.status(404).send({error: "Customer not found"});
            }
            console.log("clientToken = " + response.clientToken);
            // Send client token in response to client
            res.status(200).send({clientToken: response.clientToken});
      });
}

/*
 * Function to generate new ClientToken
 * res parameters:
     "clientToken": string
 */   
module.exports.generateNewClientToken = function(req, res) {
    gateway.clientToken.generate({
      }, function (err, response) {
            if (err || response.clientToken == undefined) {
                console.log("Failed to locate customer");
                res.status(404).send({error: "Customer not found"});
            }
            console.log("clientToken = " + response.clientToken);
            // Send client token in response to client
            res.status(200).send({clientToken: response.clientToken});
      });
}


/* Create customer with payment nonce
 *
 * Post Body Parameters:
 * payment_method_nonce
 * company_name
 * phone
 * website
 * 
 * res Parameters:
    "result": bool,
    "customerId": string,
    "paymentMethodToken": string
 */ 

module.exports.createCustomer = function(req,res) {
    gateway.customer.create({
        paymentMethodNonce: req.body.payment_method_nonce,
        company: req.body.company_name,
        phone: req.body.phone,
        website: req.body.website,
        creditCard: {
          options: {
            verifyCard: true
          }
        }
      }, function (err, result) {
          if (err || result.success == false) {
              res.status(400).send({error: result.message});
              console.log("Error: " + result.message);
              return;
          }
          console.log("Result Success = " + result.success);
          res.status(201).send({success: result.success, customerId: result.customer.id, paymentMethodToken: result.customer.paymentMethods[0].token});
      });
}

/*
 * Function to delete customer from Braintree
 * 
 * Body Parameters:
 * customer_id
 * 
    res Parameters:
    "success": bool
 */
 
module.exports.deleteCustomer = function(req,res) {
    gateway.customer.delete(req.body.customer_id, function (err) {
        if (err) {
            res.status(400).send({error: err});
            console.log("Error: " + err);
            return;
        }
        console.log("Sucessfully deleted customer object");
        res.status(200).send({success: "true"});
      });
 }

/*
 * Function to update customer information in Braintree
 * 
 * Body Parameters:
 * customer_id
 * company
 * phone
 * website
 * 
 * res parameters:
    success: bool,
    customerId: string
 */

module.exports.updateCustomer = function(req, res) {
    gateway.customer.update(req.body.customer_id, {
        company: req.body.company_name,
        phone: req.body.phone,
        website: req.body.website,
      }, function (err, result) {
        if (err || result.success == false) {
            res.status(400).send({error: err});
            console.log("Error: " + err);
            return;
        }
        console.log("Result Success = " + result.success);
        res.status(200).send({success: result.success, customerId: result.customer.id});
      });
}


/* Creates a new payment method for the customer using a payment nonce and customer ID (customer ID must be pulled from our database)
 *
 * Body Parameters:
 * customer_id
 * payment_method_nonce
 * default (make default payment method)
 * 
 * res Parameters:
    "result": bool,
    "customerId": string,
    "paymentMethodToken": string
 */

module.exports.addPaymentMethod = function(req,res) {
    gateway.paymentMethod.create({
        customerId: req.body.customer_id,
        paymentMethodNonce: req.body.payment_method_nonce,
        options: {
            makeDefault: req.body.default,
            verifyCard: true
        }
      }, function (err, result) {
        if (err || result.success == false) {
            res.status(400).send({error: result.message});
            console.log("Error: " + result.message);
            return;
        }
        console.log("Result Success = " + result.success);
        console.log("CC Token = " + result.paymentMethod.token);

        res.status(201).send({result: result.success, customerId: result.paymentMethod.customerId, paymentMethodToken: result.paymentMethod.token});
       });
}

/*
 * Deletes an existing payment method from braintree
 * 
 * Body Parameters:
 * cctoken (payment method token)
 * res Parameters:
    "success": bool
 */
 
module.exports.deletePaymentMethod = function(req,res) {
    gateway.paymentMethod.delete(req.body.cctoken, function (err) {
        if (err) {
            res.status(400).send({error: err});
            console.log("Error: " + err);
            return;
        }
        console.log("Sucessfully deleted payment method");
        res.status(200).send({success: "true"});
    });
}

/*
 * Authorizes sale to customerID and their default payment method
 * 
 * req body Parameters:
 * customer_id
 * amount
 * 
 * res Parameters:
    "success": bool
    "merchantId": string
    "customerId": string,
    "transctionId": string,
    "paymentStatus": string,
    "paymentType": string,
    "currencyIsoCode": string,
    "amount": string,
    "createdAt": string,
    "updatedAt": string
 */

module.exports.chargeCustomerId = function (req, res) {
    gateway.transaction.sale({
        customerId: req.body.customer_id,
        amount: req.body.amount
      }, function (err, result) {
        if (err || result.success == false) {
            res.status(400).send({error: result.message});
            console.log("Error: " + result.message);
            return;
        }
        console.log("Result Success = " + result.success);
        console.log("Transaction ID = " + result.transaction.id);

        res.status(200).send({success: result.success, merchantId: result.transaction.merchantAccountId, customerId: result.transaction.customer.id, transctionId: result.transaction.id, paymentStatus: result.transaction.status, paymentType: result.transaction.type, currencyIsoCode: result.transaction.currencyIsoCode, amount: result.transaction.amount, createdAt: result.transaction.createdAt, updatedAt: result.transaction.updatedAt});
      });
}

/*
 * Finds transaction, returns transactions status
 * 
 * req body parameters:
    transaction_id
 * res parameters:
    returns string with status
 */

module.exports.transactionStatus = function(req, res) {
    gateway.transaction.find(req.body.transaction_id, function (err, transaction) {
        if (err) {
            res.status(400).send({Error: err});
            return;
        }
        res.status(200).send(transaction.status);
    });
}

/*
 * Submits previously authorized transaction for settlement
 * (must be done after 72 hours of authorized transactions to avoid authorization misuse fees)
 * https://www.braintreepayments.com/blog/visa-misuse-of-authorization/
 * 
 * req Body Parameters:
 * transction_id
 * 
 * res Parameters:
    "success": bool
    "merchantId": string
    "customerId": string,
    "transctionId": string,
    "paymentStatus": string,
    "paymentType": string,
    "currencyIsoCode": string,
    "amount": string,
    "createdAt": string,
    "updatedAt": string
 */

module.exports.settleTransaction = function (req, res) {
    gateway.transaction.submitForSettlement(req.body.transaction_id, function (err, result) {
        if (err) {
            res.status(400).send({Error: err});
            return;
        }
        
        res.status(200).send({success: result.success, merchantId: result.transaction.merchantAccountId, customerId: result.transaction.customer.id, transctionId: result.transaction.id, paymentStatus: result.transaction.status, paymentType: result.transaction.type, currencyIsoCode: result.transaction.currencyIsoCode, amount: result.transaction.amount, createdAt: result.transaction.createdAt, updatedAt: result.transaction.updatedAt});
      });
}

/*
 * Voids transaction
 * 
 * req body parameters:
    transaction_id
 * res parameters:
    "void_success: ", bool
 */

module.exports.voidTransaction = function (req, res) {
    gateway.transaction.void(req.body.transaction_id, function (err, result) {
        if (err) {
            res.status(400).send({Error: err})
            return;
        }
        res.status(200).send("void_success: " + result.success);
    });
}


/*
 ************************ NON-API (AGENDA) FUNCTIONS ************************
*/

/*
 * Authorizes sale to customerID and their default payment method
 * 
 * Data Parameters:
 * customer_id
 * amount
 */

module.exports.chargeCustomerIdAgenda = function (data, callback) {
    gateway.transaction.sale({
        customerId: data.customer_id,
        amount: data.amount
      }, function (err, result) {
        if (err || result.success == false) {
            // TODO: Add deep error reporting (status codes)
            // https://developers.braintreepayments.com/reference/general/validation-errors/overview/ruby
            callback(result.message, null);
        }
        else {
            callback(null, result);
        }
      });
}

/*
 * Finds transaction, returns transactions status
 * 
 * data parameters:
    transaction_id
 * callback parameter:
    returns string with status
 */

module.exports.transactionStatusAgenda = function(data, callback) {
    gateway.transaction.find(data.transaction_id, function (err, transaction) {
        if (err) {
            // TODO: Add deep error reporting (status codes)
            // https://developers.braintreepayments.com/reference/general/validation-errors/overview/ruby
            callback(err, null);
        }
        else {
            callback(null, transaction);
        }
    });
}

/*
 * Submits previously authorized transaction for settlement
 * (must be done after 72 hours of authorized transactions to avoid authorization misuse fees)
 * https://www.braintreepayments.com/blog/visa-misuse-of-authorization/
 * 
 * Input Parameters:
 * transction_id
 */

module.exports.settleTransactionAgenda = function (data, callback) {
    gateway.transaction.submitForSettlement(data.transaction_id, function (err, result) {
        if (err) {
            // TODO: Add deep error reporting (status codes)
            // https://developers.braintreepayments.com/reference/general/validation-errors/overview/ruby
            callback(err, null);
        }
        else {
            callback(null, result)
        }
    });
}

/*
 * Voids transaction
 * 
 * Input parameters:
    transaction_id

   Returns: true or false (success/fail)
 */

module.exports.voidTransactionAgenda = function (data, callback) {
    gateway.transaction.void(data.transaction_id, function (err, result) {
        if (err) {
            // TODO: Add deep error reporting (status codes)
            // https://developers.braintreepayments.com/reference/general/validation-errors/overview/ruby
            callback(err, null);
        }
        else {
            callback(null, result.success);
        }
    });
}