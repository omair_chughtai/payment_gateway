var agenda = require('../worker').agenda;
var paymentTable = require('../models/paymentTable');

module.exports.addPayment = function(req, res) {
	var data = req.body;

	paymentTable.addPayment(data, function(err, data) {
		if (err) res.status(404).json({ success: false, message: err });
		else res.status(200).json({ success: true, data: data });
	});
};

module.exports.deletePayment = function(req, res) {
	var data = req.body;

	paymentTable.deletePayment(data, function(err, data) {
		if (err) res.status(404).json({ success: false, message: err });
		else res.status(200).json({ success: true, data: data });
	});
};

/*
 * Agenda function to remove jobs from agenda job database upon completion
 *  (Causes strange errors if not used, sometimes restarting old jobs)
 */

agenda.on('complete', function(job) {
	//    console.log("Job finished");
	job.remove(function(err) {
		//        console.log(err); //prints null
	});
});

// Agenda jobs
var chargePeriod = '15 seconds'; // Change to '0 0 1 * *' for every 1st of month
var completePeriod = '15 seconds'; // Change to '0 0 4 * *' for every 4th of month

agenda.on('ready', () => {
	console.log('other ready');
	agenda.start();
	//  agenda.every(chargePeriod, 'chargeCustomers');
	agenda.schedule('in 3 seconds', 'chargeCustomers', { attempt: 1 });
	console.log('job scheduled');
});
