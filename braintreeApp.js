// Authored by Omair Chughtai for Stratus Silver Lining

const express = require('express');
var app = express();
path = require('path');
var bodyParser = require('body-parser');
var db = require(path.join(__dirname, '/config/mongoose'));
var agenda = require('./scheduler/agenda/agenda');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

var braintreeRoutes = require(path.join(__dirname, '/routes/braintree'));
var paymentRoutes = require(path.join(__dirname, '/routes/payment'));

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Method', 'GET,POST,DELETE,OPTIONS');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-with,Content-Type, Authorization');
	next();
});

app.use(braintreeRoutes);
app.use(paymentRoutes);
app.use(express.static(path.join(__dirname, '/public')));

var listener = app.listen(3000, function() {
	console.log('listening on port ' + listener.address().port);
});
